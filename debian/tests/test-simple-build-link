#!/bin/sh

set -e
at_exit() {
    set +x
    echo "info: test exiting, removing temp files"
    rm -f test-simple-build-link.ogg test-simple-build-link.c \
      test-simple-build-link-prog
}
trap at_exit INT TERM EXIT
set -x

cd $AUTOPKGTEST_TMP

if type xvalgrind >/dev/null 2>&1 ; then
    VALGRIND=valgrind
fi

head -c 500000 /dev/zero | oggenc -Q -r -o test-simple-build-link.ogg -

# This is just a simple code example to compile to verify the headers
# exist, are includable and the library is linkable.  It should be
# extended to really use the library to test a bit more of the
# package.
cat > test-simple-build-link.c <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <oggz/oggz.h>
int
main (int argc, char ** argv)
{
  OGGZ * reader;

  /* quiet down compiler about unused arguments */
  if (1 < argc)
      return (strlen(argv[1]) != 0);

  reader = oggz_open("test-simple-build-link.ogg", OGGZ_READ);
  if ( oggz_run (reader) != 0 ) {
    fprintf(stderr, "error: oggz_run did not complete");
    exit (1);
  }
  if ( oggz_close (reader) != 0 ) {
    fprintf(stderr, "error: oggz_close did not complete");
    exit (1);
  }
  printf("info: program ran as it should\n");

  exit (0);
}
EOF

gcc -o test-simple-build-link-prog -Wall -Wextra test-simple-build-link.c -loggz

${VALGRIND} ./test-simple-build-link-prog
