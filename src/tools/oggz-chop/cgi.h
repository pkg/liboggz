#ifndef OGGZ_CHOP_CGI_HEADER
#define OGGZ_CHOP_CGI_HEADER

#include "oggz-chop.h"

int cgi_test (void);

int cgi_main (OCState * state);

#endif /* OGGZ_CHOP_CGI_HEADER */
