#ifndef HTTPDATE_HEADER
#define HTTPDATE_HEADER

#include <time.h>

void httpdate_init (void);
int httpdate_snprint (char * buf, int n, time_t mtime);
time_t httpdate_parse (char * s, int n);

#endif /* HTTPDATE_HEADER */
